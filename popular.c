/* Popular file words */

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
static char* FEW_ARG_ERR = "Usage: popular file-name and words-len\n";
static int SIZE_BUF = 64;

void out() {
    char *str = malloc(SIZE_BUF);
    int key = 1;
    int j = 1;
    int i = 0;
    for (i; read(0, str+i, 1) == 1; i++) {
        if (str[i] == 10) {
            if (key) {
                str[i] = '\t';
                key = 0;
            } else {
                key = 1;
            }
        }
        if (i == SIZE_BUF) {
            str = realloc(str, SIZE_BUF * ++j);
        }
    }
    str[i-1] = '\0';
    /*for (i = 0; i < strlen(str); i++) {
        printf("%d\n", str[i]);
    }*/
    printf("%s\n", str);
    return;
}


int main(int argc, char *argv[]) {
    int fdp1[2];
    int fdp2[2];
    int fdp3[2];
    int fdp4[2];
    int fdp5[2];
    char* exp;
    int len_arg;

    pipe(fdp1);
    pipe(fdp2);
    pipe(fdp3);
    pipe(fdp4);
    pipe(fdp5);

    if (argc < 1) {
        write(1, FEW_ARG_ERR, strlen(FEW_ARG_ERR));
        exit(0);
    }

    len_arg = strlen(argv[2]);
    exp = malloc(len_arg + 12);
    memcpy(exp, "^[A-Za-z]{", 10);
    memcpy(exp + 10, argv[2],  len_arg);
    memcpy(exp + 10 + len_arg, "}$", 2);

    if (fork() == 0) {
        close(1);
        dup(fdp1[1]);
        close(fdp1[1]); close(fdp1[0]);
        execl("/bin/cat", "cat", argv[1], (char *) 0);
        exit(1);
    }

    if (fork() == 0) {
        close(0);
        dup(fdp1[0]);
        close(fdp1[0]); close(fdp1[1]);
        close(1);
        dup(fdp2[1]);
        close(fdp2[1]);
        execl("/usr/bin/tr", "tr", "-cs", "A-Za-z", "\n", (char *) 0);
        exit(2);
    }

    if (fork() == 0) {
        close(0);
        dup(fdp2[0]);
        close(fdp2[0]); close(fdp2[1]);
        close(fdp1[0]); close(fdp1[1]);
        close(1);
        dup(fdp3[1]);
        close(fdp3[1]); close(fdp3[0]);
//        execl("/usr/bin/sort", "sort", (char *) 0);
        execl("/bin/egrep", "egrep", exp, (char *) 0);
        exit(3);
    }

    if (fork() == 0) {
        close(0);
        dup(fdp3[0]);
        close(fdp3[0]); close(fdp3[1]);
        close(fdp1[0]); close(fdp1[1]);
        close(fdp2[0]); close(fdp2[1]);
        close(1);
        dup(fdp4[1]);
        close(fdp4[1]);
        execl("/usr/bin/uniq", "uniq", "-c", (char *) 0);
        exit(4);
    }

    if (fork() == 0) {
        close(0);
        dup(fdp4[0]);
        close(fdp4[0]); close(fdp4[1]);
        close(fdp1[0]); close(fdp1[1]);
        close(fdp2[0]); close(fdp2[1]);
        close(fdp3[0]); close(fdp3[1]);
        close(1);
        dup(fdp5[1]);
        close(fdp5[1]);
        execl("/usr/bin/sort", "sort", "-n", (char *) 0);
        exit(5);
    }

    if (fork() == 0) {
        close(0);
        dup(fdp5[0]);
        close(fdp5[0]); close(fdp5[1]);
        close(fdp1[0]); close(fdp1[1]);
        close(fdp2[0]); close(fdp2[1]);
        close(fdp3[0]); close(fdp3[1]);
        close(fdp4[0]); close(fdp4[1]);
//        execl("/bin/cat", "cat", (char*) 0);
        out();
//        execl("/usr/bin/tail", "tail", "-n", argv[2], (char *) 0);
        exit(6);
    }

    close(fdp1[0]); close(fdp1[1]);
    close(fdp2[0]); close(fdp2[1]);
    close(fdp3[0]); close(fdp3[1]);
    close(fdp4[0]); close(fdp4[1]);
    close(fdp5[0]); close(fdp5[1]);

    while (wait(0) != (-1));
    exit(0);
}
